## Operação Leva Jeito

O programa usa cálculo de puro Hash ou HMAC, para garantir a autenticação de um conjunto de arquivos para uma determinada pasta (recursivamente). 

---

### Considerações Gerais:

O programa permite ter um controle de uma pasta (recursivamente), verificando adições, alterações e remoções de arquivos nesta pasta.

Sobre os comandos que o Makefile permite:

* "make clean" = apaga os arquivos objeto e binário;
* "make" = compila o programa;
* "make doc" = gera a documentação do programa, em que será possivel visualizar acesssando a pasta doc e abrindo o arquivo index.html.

---

### Para utilizar o programa:

O programa deverá ser executado em linha de comando, em um sistema baseado em UNIX (todos dentro da pasta principal do programa):

* Compile o programa, usando o comando "make";
* Execute passando os parâmetros corretos: "./bin/guarda <metodo> <opcao> <pasta> <saída>"
    * **<metodo>** : indica o método a ser utilizado ( -hash ou -hmac senha)
        * Exemplo de uso do '-hash': "./bin/guarda -hash -i ./teste"
        * Exemplo de uso do '-hmac': "./bin/guarda -hmac senha -i ./teste"
    * **<opcao>**: indica a ação a ser desempenhada pelo programa;
      * **-i**: inicia a guarda da pasta indicada em <pasta>, ou seja, faz a leitura de todos os arquivos da pasta (recursivamente) registrando os dados e Hash/HMAC de cada um e armazenando numa estrutura própria;
        * Nesta opção não deve ser passado a <saida>, exemplo de uso seria: "./bin/guarda -hash -i ./teste" 
      * **-t**: faz o rastreio (tracking) da pasta indicada em <pasta>, inserindo informações sobre novos arquivos e indicando
      alterações detectadas/exclusões;
        * Nesta opcão o uso da <saida> é opcional.
          * Exemplo de uso com <saida>: "./bin/guarda -hmac senha ./teste info.log";
          * Exemplo de uso sem <saida>: "./bin/guarda -hmac senha ./teste"
      * **-x**: desativa a guarda e remove a estrutura alocada.
        * Nesta opção a estrutura salva será removida, um exemplo de uso seria "./bin/guarda -hash -x ./teste"
    * **<pasta>**: indica a pasta a ser “guardada”;
      * Obs: Para passar a pasta como parâmetro, não utilize '/' no final, pois isso pode causar problemas no programa.
          * Uso correto: "./bin/guarda -hmac senha -i ./teste"
          * Uso incorreto: "./bin/guarda -hmac senha -i ./teste/"
    * **<saida>**: indica o arquivo de saída para o relatório. Caso não seja passado este parâmetro, a saída será feita na saída padrão do programa (terminal).
        * Este parâmetro só é aceito na opção '-t'.