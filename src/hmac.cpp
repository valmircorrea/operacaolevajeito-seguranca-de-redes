/**
* @file	    hmac.cpp
* @brief	Implementação das funções que realizam o cálculo do HMAC com Hash MD5
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#include "hmac.h"
#include "hl_exception.h"
#include "hl_hashwrapper.h"
#include "hl_md5.h"
#include "hl_md5wrapper.h"
#include "hl_types.h"

#include <bitset>
using std::bitset;

#include <sstream>
using std::istringstream;

#include <string>
using std::string;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <ostream>
using std::ifstream;

/**
* @brief    Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param    nome_arq Nome do arquivo a ser lido
* @return   Mensagem lida do arquivo
*/
string ler_msg (string nome_arq)
{
    ifstream arquivo (nome_arq);
    if (!arquivo)
    {
        cerr << "--> Erro ao ler mensagem !" << endl;
        exit (1);
    }

    string mensagem;
    while (!arquivo.eof()) //enquanto end of file for false continua
    {
        string str_temp;
        getline (arquivo, str_temp);
        mensagem += str_temp;
        mensagem += "\n";
    }

    mensagem.erase(mensagem.end()-1, mensagem.end());

    arquivo.close();

    return mensagem;
}

/**
* @brief    Função que realiza o cálculo do Hash Mac com o hash MD5
* @param    chave Chave para calcular o hash
* @param    arquivo Arquivo passado para calcular seu hash
* @return   HMAC do arquivo passado
*/
string hmac (string chave, string arquivo)
{
    string mensagem = ler_msg (arquivo);

    hashwrapper *myWrapper = new md5wrapper(); 
    try {
        myWrapper->test();
    } catch(hlException &e) {
        //your error handling here
    }

    int blocksize = 512 / 8;
    int aux;

    if ((int) chave.size() > blocksize) {
        try {
            chave = myWrapper->getHashFromString(chave);
            
            string chave_temp;
            for (int ii = 0; ii < (int) chave.size(); ii += 2) {
                char hex[2] = {chave[ii], chave[ii+1]};
                istringstream(hex) >> std::hex >> aux;
                
                chave_temp += aux;
            }

            chave = chave_temp;
        } catch(hlException &e) {
            //your error handling here
        }
    }
    
    if ((int) chave.size() < blocksize) {
        int preenchimento = blocksize - chave.size();
        for (int ii = 0; ii < preenchimento; ii++) {
            chave += char (0);
        }
    }
    
    string o_key_pad;
    string i_key_pad;
    for (int ii = 0; ii < blocksize; ii ++) {
        bitset <8> opad (0x5c);
        bitset <8> ipad (0x36);
        bitset <8> chave_bit (chave[ii]);

        o_key_pad += (opad ^= chave_bit).to_ulong();
        i_key_pad += (ipad ^= chave_bit).to_ulong();
    }

    string temp = myWrapper->getHashFromString(i_key_pad + mensagem);

    // Separa a hash em partes hexadecimais para concatenar com o o_key_pad
    for (int ii = 0; ii < (int) temp.size(); ii += 2) {
        char hex[2] = {temp[ii], temp[ii+1]};
        istringstream(hex) >> std::hex >> aux;
        
        o_key_pad += aux;
    }

    string hmac = myWrapper->getHashFromString (o_key_pad);

    delete myWrapper;

    return hmac;
}