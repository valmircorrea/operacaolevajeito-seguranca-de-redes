/**
* @file	    main.cpp
* @brief	Arquivo com a função principal do programa.
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include "config.h"
#include "manip_arq.h"
#include "rastreio.h"
#include "manip_xml.h"

/**
* @brief    Função que verifica se os argumentos passados ao programa são válidos
* @param    argc Números de argumentos passados
* @param    argv Vetor com os argumentos passados
* @param    config Variável que deve conter as configurações do programa
*/
void verifica_args (int argc, char* argv[], Config &config) {

    // Verifica se são passados os argumentos corretamente
    if (argc < 4 || argc > 7) {
        cout << "--> Argumentos invalidos! Use: './guarda <metodo> <opcao> <pasta> <saída>'" << endl;
        cout << "   ― <metodo> : Indica o método a ser utilizado ( -hash ou -hmac senha) " << endl;
        cout << "   ― <opcao>  : Indica a ação a ser desempenhada pelo programa " << endl;
        cout << "        • -i : Inicia a guarda da pasta indicada em <pasta> " << endl;
        cout << "        • -t : Faz o rastreio (tracking) da pasta indicada em <pasta> " << endl;
        cout << "        • -x : Desativa a guarda e remove a estrutura alocada " << endl;
        cout << "   ― <pasta>  : Indica a pasta a ser “guardada” " << endl;
        cout << "   ― <saida>  : Indica o arquivo de saída para o relatório (-o <saída>).";
        cout << " Caso não seja passado este parâmetro, a saída será feita pelo terminal." << endl;
        exit(1);
    }

    // Configurações iniciais
    config.metodo = argv[1];

    if (config.metodo != "-hash" && config.metodo != "-hmac") {
        cout << "--> Metódo inválido! " << endl;
        cout << "   ― <metodo> : Indica o metódo a ser usado pelo programa " << endl;
        cout << "       • -hash : Utiliza Hash MD5" << endl;
        cout << "       • -hmac <chave> : Utiliza HMAC com a <chave> passada " << endl;
        exit(1);
    }

    if (config.metodo == "-hmac") {
        if (argc < 5) {
            cout << "--> Argumentos invalidos! Use: './guarda <metodo> <opcao> <pasta> <saída>'" << endl;
            cout << "   ― <metodo> : Indica o método a ser utilizado ( -hash ou -hmac senha) " << endl;
            cout << "   ― <opcao>  : Indica a ação a ser desempenhada pelo programa " << endl;
            cout << "        • -i : Inicia a guarda da pasta indicada em <pasta> " << endl;
            cout << "        • -t : Faz o rastreio (tracking) da pasta indicada em <pasta> " << endl;
            cout << "        • -x : Desativa a guarda e remove a estrutura alocada " << endl;
            cout << "   ― <pasta>  : Indica a pasta a ser “guardada” " << endl;
            cout << "   ― <saida>  : Indica o arquivo de saída para o relatório (-o <saída>).";
            cout << " Caso não seja passado este parâmetro, a saída será feita pelo terminal." << endl;
            exit(1);
        }

        config.chave = argv[2];    
        config.opcao = argv[3];
        config.pasta = argv[4];
    } else {
        config.opcao = argv[2];
        config.pasta = argv[3];
    }

    if (config.opcao == "-i") {
        if (argc != 4 && config.metodo == "-hash") {
            cout << "--> Argumentos invalidos!" << endl;
            cout << "  ― Para a opção -i não é necessário informar a saída." << endl;
            exit(1);
        } else if (argc != 5 && config.metodo == "-hmac") {
            cout << "--> Argumentos invalidos!" << endl;
            cout << "  ― Para a opção -i não é necessário informar a saída." << endl;
            exit(1);
        }
    } else if (config.opcao == "-t") {

        if (config.metodo == "-hash") {
            if (argc == 5) {
                config.saida = argv[4];
            }
        } else {
            if (argc == 6) {
                config.saida = argv[5];
            }
        }
    } else if (config.opcao == "-x") {
        // if (argc != 2) {
        //     cout << "--> Argumentos invalidos!" << endl;
        //     cout << "  ― Para a opção -x basta passar a pasta como parâmetro" << endl;
        //     exit(1);
        // }
        if (argc != 4 && config.metodo == "-hash") {
            cout << "--> Argumentos invalidos!" << endl;
            cout << "  ― Para a opção -x não é necessário informar a saída." << endl;
            exit(1);
        } else if (argc != 5 && config.metodo == "-hmac") {
            cout << "--> Argumentos invalidos!" << endl;
            cout << "  ― Para a opção -x não é necessário informar a saída." << endl;
            exit(1);
        }
    } else {
        cout << "--> Opção inválida! " << endl;
        cout << "   ― <opcao> : Indica a ação a ser desempenhada pelo programa " << endl;
        cout << "       • -i : Inicia a guarda da pasta indicada em <pasta> " << endl;
        cout << "       • -t : Faz o rastreio (tracking) da pasta indicada em <pasta> " << endl;
        cout << "       • -x : Desativa a guarda e remove a estrutura alocada " << endl;
        exit(1);
    }
}

/**
* @brief    Função principal do programa
*/
int main (int argc, char* argv[]) {
    
    Config config;

    verifica_args (argc, argv, config);

    list<Dados> arquivos;

    string baseDados = config.pasta + "/.rastreio.xml";

    // Inicia o tipo de opção desejada:
    if (config.opcao == "-i") {                 // varredura dos diretorios atuais
        
        if (config.chave == "-hash") {
            listaArquivos(config.pasta, arquivos, config.metodo);
        } else {
            listaArquivos(config.pasta, arquivos, config.metodo, config.chave);
        }

        save (baseDados.c_str(), arquivos);

        cout << "--> Guarda iniciada com sucesso !" << endl;
    
    } else if (config.opcao == "-t") {          // rastreio para verificar as mudanças

        list<Dados> arquivos_antigos;
        list<Dados> arquivos_atual;

        load(baseDados.c_str(), arquivos_antigos);

        if (config.metodo == "-hash") {
            listaArquivos(config.pasta, arquivos_atual, config.metodo);
        } else {
            listaArquivos(config.pasta, arquivos_atual, config.metodo, config.chave);
        }

        rastreio(arquivos_antigos, arquivos_atual, config.saida);

        save (baseDados.c_str(), arquivos_atual);

        cout << endl << "--> Guarda atualizada !" << endl;

    } else if (config.opcao == "-x") {          // desativa a guarda e remove a estrutura
        if (system (("rm " + baseDados + " 2>/dev/null").c_str()) != 0) {
            cerr << "--> Erro ao remover a estrutura alocada ou estrutura não existe!" << endl;
        } else {
            cout << "--> Estrutura removida com sucesso !" << endl;
        }
    }

    return 0;
}