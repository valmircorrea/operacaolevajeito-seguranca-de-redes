/**
* @file	    log.cpp
* @brief	Implementação das funções que mostram/gravam os logs do rastreio
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#include "log.h"

#include <iostream>
using std::endl;
using std::cerr;
using std::cout;

#include <string>
using std::string;

#include <ctime>

#include <fstream>
using std::ofstream;

/**
* @brief    Função que grava a mensagem cifrada em um arquivo
* @param    mensagem Mensagem cifrada a ser gravada no arquivo
* @param    nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq)
{
    ofstream arquivo;
    arquivo.open (nome_arq, std::ofstream::out | std::ofstream::app);

    if (!arquivo)
    {
        cerr << "--> Erro ao gravar log !" << endl;
        exit (1);
    }

    arquivo << mensagem;
    arquivo.close ();
}

/**
* @brief    Imprimi/grava a mensagem de log
* @param    saida Saida para o log, se for "default", sairá na saída padrão do programa
* @param    msg Mensagem de log
*/
void log (string saida, string msg) {

    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    string date = asctime(timeinfo);

    string log = "[" + date;
    log.erase(log.end()-1, log.end());
    log += "] - ";

    log += msg;

    if (saida == "default") {
        cout << log;
    } else {
        gravar_msg (log, saida);
    }
}