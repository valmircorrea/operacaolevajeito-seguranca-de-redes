/**
* @file	    manip_xml.cpp
* @brief	Implementação das funções manipulam os arquivos xml
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#include "manip_xml.h"

#include "tinyxml.h"
#include "tinystr.h"
#include "dados.h"

#include <list>
using std::list;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

/**
* @brief    Função auxiliar para gravar os dados de cada 
            elemento em um arquivo XML.
* @param    root Elemento de um xml
* @param    arquivos Estrutura com os arquivos
*/
 void gravar (TiXmlElement &root, list<Dados> arquivos)
 {
     for (auto it = arquivos.begin(); it != arquivos.end(); it++) {
        if (it->nome[0] == '.') {
            string temp = "oculto";
            temp += it->nome;
            it->nome = temp;
        }

        if (it->nome[0] >= 48 && it->nome[0] <= 57) {
            string temp = "_";
            temp += it->nome;
            it->nome = temp;
        }

        if (it->isFile) {
            TiXmlElement *arquivo;
            arquivo = new TiXmlElement (it->nome.c_str());
            arquivo->LinkEndChild (new TiXmlText (it->hash.c_str()));
            root.LinkEndChild (arquivo);
        } else {
            TiXmlElement *pasta = new TiXmlElement(it->nome.c_str());
            root.LinkEndChild (pasta);
            gravar (*pasta, it->lista);
        }
    }
 }

/**
* @brief    Salva a estrutura em um arquivo XML
* @param    pFilename Nome do arquivo a ser salvo
* @param    arquivos Estrutura com os arquivos
*/
void save(const char* pFilename, list<Dados> arquivos) {

	TiXmlDocument doc;              // cria documento  
 	TiXmlDeclaration *decl = new TiXmlDeclaration( "1.0", "", "" );  
	doc.LinkEndChild(decl); 
 
    string m_name = "ARQUIVOS";
	TiXmlElement *root = new TiXmlElement(m_name.c_str());  
	doc.LinkEndChild(root); 

    gravar (*root, arquivos);

	doc.SaveFile(pFilename);
}

/**
* @brief    Função auxiliar para leitura de cada elemento de um arquivo XML.
* @param    pElem Elemento de um xml
* @param    arquivos Estrutura com os arquivos
*/
void ler (TiXmlElement *pElem, list<Dados> &arquivos) {
    for( ; pElem; pElem=pElem->NextSiblingElement())
    {
        const char *pKey=pElem->Value();
        const char *pText=pElem->GetText();

        if (pKey && pText) 
        {
            Dados temp;
            temp.isFile = true;
            temp.nome = pKey;
            temp.hash = pText;

            if (temp.nome.length() >= 6) {
                char aux[6];
                strncpy(aux, temp.nome.c_str(), 6);      
                if (strcmp(aux, "oculto") == 0) {
                    string str = temp.nome;
                    str.erase (0, 6);
                    temp.nome = str; 
                }
            }
            
            if (temp.nome[0] == '_') {
                string str = temp.nome;
                str.erase (0, 1);
                temp.nome = str; 
            }

            arquivos.push_back(temp);
        } else if (pKey) { 
            Dados temp;
            temp.nome = pKey;
            temp.isFile = false;

            if (temp.nome.length() >= 6) {
                char aux[6];
                strncpy(aux, temp.nome.c_str(), 6);     
                if (strcmp(aux, "oculto") == 0) {
                    string str = temp.nome;
                    str.erase (0, 6);
                    temp.nome = str; 
                }
            }
            
            if (temp.nome[0] == '_') {
                string str = temp.nome;
                str.erase (0, 1);
                temp.nome = str; 
            }

            if (pElem->FirstChildElement()) {
                ler (pElem->FirstChildElement()->ToElement(), temp.lista);
            }

            arquivos.push_back(temp);
        }
    }
}

/**
* @brief    Faz a leitura dos dados/estrutura de um arquivo XML.
* @param    pFilename Nome do arquivo a ser carregado
* @param    arquivos Lista de arquivos
*/
void load(const char* pFilename, list<Dados> &arquivos) {

	TiXmlDocument doc (pFilename);
    bool loadOkay = doc.LoadFile();

	if (!loadOkay) {
        cerr << "--> Erro ao ler estrutura !" << endl;
        exit(1);
    }

	TiXmlHandle hDoc(&doc);
	TiXmlElement* pElem;
	TiXmlHandle hRoot(0);

    pElem=hDoc.FirstChildElement().Element();

    // should always have a valid root but handle gracefully if it does
    if (!pElem) return;
    string m_name = pElem->Value();

    // save this for later
    hRoot=TiXmlHandle(pElem);

    pElem=hRoot.FirstChildElement().Element();
    ler (pElem, arquivos);
}