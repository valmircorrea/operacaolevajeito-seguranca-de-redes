/**
* @file	    rastreio.cpp
* @brief	Implementação das funções que realizam o rastreio do diretorio/arquivo
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#include "rastreio.h"

#include "dados.h"
#include "log.h"

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <string>
using std::string;

#include <iterator>
using std::iterator;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

/**
* @brief    Função que verifica se um arquivo foi adicionado ou removido da estrutura
* @param    arquivos Lista dos arquivos para verificação
* @param    dado Arquivo para verificação
* @param    dir Diretorio do arquivo em que a busca está sendo realizada
* @param    count Contador que auxilia em qual subpasta do diretório a busca está sendo realizada
* @return   Se o arquivo foi adicionado ou removido, dependendo da chamada realizada
*/
bool verifica_adicao_remocao_aux (list<Dados> arquivos, Dados dado, vector<string> dir, int count)
{
    for (auto it = arquivos.begin(); it != arquivos.end(); it++)
    {
        if (count == (int) dir.size()) {
            if (it->isFile && (it->nome == dado.nome)) {
                return false;
            }
        } else if (!it->isFile && (it->nome == dir[count])) {
            count++;
            return verifica_adicao_remocao_aux (it->lista, dado, dir, count);
        }
    }

    return true;
}

/**
* @brief    Função que verifica se um arquivo foi alterado na estrutura
* @param    arquivos Lista dos arquivos para verificação
* @param    dado Arquivo para verificação
* @param    dir Diretorio do arquivo em que a busca está sendo realizada
* @param    count Contador que auxilia em qual subpasta do diretório a busca está sendo realizada
* @return   Se o arquivo foi alterado ou não
*/
bool verifica_alteracao_aux (list<Dados> arquivos, Dados dado, vector<string> dir, int count)
{
    for (auto it = arquivos.begin(); it != arquivos.end(); it++)
    {
        if (count == (int) dir.size()) {
            if (it->isFile && (it->nome == dado.nome)) {
                if (it->hash == dado.hash) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (!it->isFile && (it->nome == dir[count])) {
            count++;
            return verifica_alteracao_aux (it->lista, dado, dir, count);
        }
    }

    return false;
}

/**
* @brief    Função que verifica as adiçoes e alterações de arquivos dos arquivos antigos para os atuais
* @param    antigos Arquivos da base de dados
* @param    atuais Arquivos atuais
* @param    saida_log Saida do log de rastreio
* @param    dir Diretorio atual da verificação
*/
void verifica_adicao_alteracao (list<Dados> antigos, list<Dados> atuais, string saida_log, vector<string> dir) 
{
    for (auto it = atuais.begin(); it != atuais.end(); it++)
    {
        if (it->isFile) {
            if (verifica_adicao_remocao_aux (antigos, *it, dir)) {
                string msg_log = "Adição: ";
                for (int ii = 0; ii < (int) dir.size(); ii++) {
                    msg_log += dir[ii] + "/";
                } 
                msg_log += it->nome + '\n';

                log (saida_log, msg_log);
            } else if (verifica_alteracao_aux (antigos, *it, dir)) {
                string msg_log = "Alteração: ";
                for (int ii = 0; ii < (int) dir.size(); ii++) {
                    msg_log += dir[ii] + "/";
                } 
                msg_log += it->nome + '\n';

                log (saida_log, msg_log);
            }
        } else {
            dir.push_back (it->nome);
            verifica_adicao_alteracao (antigos, it->lista, saida_log, dir);
            dir.pop_back();
        }
    }
}

/**
* @brief    Função que verifica as remoções de arquivos dos arquivos antigos para os atuais
* @param    antigos Arquivos da base de dados
* @param    atuais Arquivos atuais
* @param    saida_log Saida do log de rastreio
* @param    dir Diretorio atual da verificação
*/
void verifica_remocao (list<Dados> antigos, list<Dados> atuais, string saida_log, vector<string> dir) 
{
    for (auto it = antigos.begin(); it != antigos.end(); it++)
    {
        if (it->isFile) {
            if (verifica_adicao_remocao_aux (atuais, *it, dir)) {
                string msg_log = "Remoção: ";
                for (int ii = 0; ii < (int) dir.size(); ii++) {
                    msg_log += dir[ii] + "/";
                } 
                msg_log += it->nome + '\n';

                log (saida_log, msg_log);
            }
        } else {
            dir.push_back (it->nome);
            verifica_remocao (it->lista, atuais, saida_log, dir);
            dir.pop_back();
        }
    }
}

/**
* @brief    Realiza o rastreio dos arquivos, verificando se houve
            adição, remoção e alteração de arquivos
* @param    arquivos_antigos Arquivos da base de dados
* @param    arquivos_atual Arquivos atuais
* @param    saida_log Saida do log de rastreio
*/
void rastreio(list<Dados> arquivos_antigos, list<Dados> arquivos_atual, string saida_log)
{
    if (saida_log == "default") {
        cout << "--> Logs: " << endl << endl;
    }

    verifica_adicao_alteracao (arquivos_antigos, arquivos_atual, saida_log);
    verifica_remocao (arquivos_antigos, arquivos_atual, saida_log);

    if (saida_log != "default") {
        cout << "--> Log gerado com sucesso, verifique o arquivo passado como saida !" << endl;
    }
}