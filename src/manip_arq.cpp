/**
* @file	    manip_arq.cpp
* @brief	Implementação das funções que manipulam os arquivos e seus dados
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#include "manip_arq.h"

#include "hl_exception.h"
#include "hl_hashwrapper.h"
#include "hl_md5.h"
#include "hl_md5wrapper.h"
#include "hl_types.h"
#include "dados.h"
#include "hmac.h"

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

#include <string>
using std::string;

#include <list>
using std::list;

#include <iterator>
using std::iterator;

#include <vector>
using std::vector;

#include <cstring>
using std::strcmp;

#include <dirent.h>
#include <cstdlib>

/**
* @brief    Função que realiza a separação de uma string através de um delimitador
* @param    texto Texto que será separado
* @param    delimitador Delimitador utilizado
* @return   Vector com todas as palavras separadas pelo delimitador, 
            em que cada indice do vector é uma palavra
*/
vector<string> dividir_string (string texto, string delimitador) {
    vector<string> separados;

    size_t pos = texto.find(delimitador);
    string aux;
    while (pos != string::npos) {
        aux = texto.substr(0, pos);
        separados.push_back(aux);
        texto.erase(0, pos + delimitador.length());

        pos = texto.find(delimitador);
    }
    separados.push_back(texto);

    return separados;
}

/**
* @brief    Imprime a lista de arquivos
* @param    arquivos lista de arquivos
*/
void imprimir (list<Dados> arquivos) {
    for (list<Dados>::iterator it = arquivos.begin(); it != arquivos.end(); it++) {
        if (it->isFile) {
            cout << "Arquivo: " << it->nome << " | " << it->hash << endl;
        } else {
            cout << endl << "Diretorio: " << it->nome << endl;
            imprimir(it->lista);
        }
    }
}

/**
* @brief    Realiza a varredura do dretorio passado para calcular os hashs 
            de cada arquivo e salvar em uma estrutura de dados
* @param    nomeDir Diretorio a ser varrido
* @param    arquivos Estrutura dos arquivos e suas informações
* @param    tipo_hash Tipo de hash que será usado, hash md5 ou hmac md5
* @param    chave Chave usada para o caso de ser hmac, caso contrário recebe nulo
*/
void listaArquivos (string nomeDir, list<Dados> &arquivos, string tipo_hash, string chave) {
    DIR *dir = 0;
    struct dirent *entrada = 0;
    unsigned char isFile = 0x8;
     
    dir = opendir (nomeDir.c_str());
 
    if (dir == 0) {
        cout << "--> Erro ao ler diretorio !" << endl;
        exit(1);
    }

    Dados temp;
    temp.isFile = false;
    vector <string> nomes = dividir_string (nomeDir, "/");
    temp.nome = nomes.back();
    arquivos.push_back(temp);

    //Iterar sobre o diretorio
    while ((entrada = readdir (dir))) {
        
        list<Dados>::iterator it;

        for (it = arquivos.begin(); it != arquivos.end(); it++) {
            if (it->nome == nomes.back()) {
                break;
            }
        }

        if (entrada->d_type == isFile) {

            if (strcmp (entrada->d_name, ".rastreio.xml") != 0) {
                Dados arquivo;
                arquivo.isFile = true;
                arquivo.nome = entrada->d_name;
                // arquivo.pai = &(*it);

                hashwrapper *myWrapper = new md5wrapper(); 
                try {
                    myWrapper->test();
                } catch(hlException &e) {
                    //your error handling here
                }

                if (tipo_hash == "-hash") {
                    try {
                        arquivo.hash = myWrapper->getHashFromFile(nomeDir + "/" + entrada->d_name);
                    } catch(hlException &e) {
                        //your error handling here
                    }
                } else if (tipo_hash == "-hmac") {
                    arquivo.hash = hmac (chave, nomeDir + "/" + entrada->d_name);
                }
                
                
                it->lista.push_back(arquivo);

                delete myWrapper;
            }
        } else {
            if ((strcmp(entrada->d_name,".") != 0) && (strcmp(entrada->d_name,"..") != 0)) {
                listaArquivos(nomeDir + "/" + entrada->d_name, it->lista, tipo_hash, chave);
            }
        }
    }
 
    closedir (dir);
}