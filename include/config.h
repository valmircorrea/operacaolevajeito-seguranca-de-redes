/**
* @file	    config.h
* @brief	Declaração da estrutura que armazena os parâmetros necessários para execução do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef CONFIG_H
#define CONFIG_H

#include <string>
using std::string;

/**
* Estrutura que armazena os parâmetros necessários para execução do programa
*/
typedef struct Config {
    string metodo = "";         /**< Metodo de hash */  
    string chave = "";          /**< Chave para hash hmac */  
    string opcao  = "";         /**< Opção de execução do programa (-i, -t, -x) */  
    string pasta  = "";         /**< Pasta desejada */  
    string saida  = "default";  /**< Saida para o log */  
} Config;

#endif