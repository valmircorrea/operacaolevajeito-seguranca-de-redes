/**
* @file	    hmac.h
* @brief	Declaração do prototipo das funções que realizam o cálculo do HMAC com Hash MD5
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef HMAC_H
#define HMAC_H

#include <string>
using std::string;

/**
* @brief    Função que recebe o nome/local do arquivo em que se encontra a mensagem
* @param    nome_arq Nome do arquivo a ser lido
* @return   Mensagem lida do arquivo
*/
string ler_msg (string nome_arq);


/**
* @brief    Função que realiza o cálculo do Hash Mac com o hash MD5
* @param    chave Chave para calcular o hash
* @param    arquivo Arquivo passado para calcular seu hash
* @return   HMAC do arquivo passado
*/
string hmac (string chave, string arquivo);

#endif