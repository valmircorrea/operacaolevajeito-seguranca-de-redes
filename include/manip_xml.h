/**
* @file	    manip_xml.h
* @brief	Declaração dos protótipos das funções manipulam os arquivos xml
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef MANIP_XML_h
#define MANIP_XML_h

#include "tinyxml.h"
#include "tinystr.h"
#include "dados.h"

#include <list>
using std::list;

/**
* @brief    Função auxiliar para gravar os dados de cada 
            elemento em um arquivo XML.
* @param    root Elemento de um xml
* @param    arquivos Estrutura com os arquivos
*/
void gravar (TiXmlElement &root, list<Dados> arquivos);

/**
* @brief    Salva a estrutura em um arquivo XML
* @param    pFilename Nome do arquivo a ser salvo
* @param    arquivos Estrutura com os arquivos
*/
void save(const char* pFilename, list<Dados> arquivos);

/**
* @brief    Função auxiliar para leitura de cada elemento de um arquivo XML.
* @param    pElem Elemento de um xml
* @param    arquivos Estrutura com os arquivos
*/
void ler (TiXmlElement *pElem, list<Dados> &arquivos);

/**
* @brief    Faz a leitura dos dados/estrutura de um arquivo XML.
* @param    pFilename Nome do arquivo a ser carregado
* @param    arquivos Lista de arquivos
*/
void load(const char* pFilename, list<Dados> &arquivos);

#endif