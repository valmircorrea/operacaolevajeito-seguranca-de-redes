/**
* @file	    manip_arq.h
* @brief	Declaração do protótipo das funções que manipulam os arquivos e seus dados
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef MANIP_ARQ_H
#define MANIP_ARQ_H

#include <string>
using std::string;

#include <list>
using std::list;

#include <vector>
using std::vector;

#include "dados.h"

/**
* @brief    Função que realiza a separação de uma string através de um delimitador
* @param    texto Texto que será separado
* @param    delimitador Delimitador utilizado
* @return   Vector com todas as palavras separadas pelo delimitador, 
            em que cada indice do vector é uma palavra
*/
vector<string> dividir_string (string texto, string delimitador);

/**
* @brief    Imprime a lista de arquivos
* @param    arquivos lista de arquivos
*/
void imprimir (list<Dados> arquivos);

/**
* @brief    Realiza a varredura do dretorio passado para calcular os hashs 
            de cada arquivo e salvar em uma estrutura de dados
* @param    nomeDir Diretorio a ser varrido
* @param    arquivos Estrutura dos arquivos e suas informações
* @param    tipo_hash Tipo de hash que será usado, hash md5 ou hmac md5
* @param    chave Chave usada para o caso de ser hmac, caso contrário recebe nulo
*/
void listaArquivos (string nomeDir, list<Dados> &arquivos, string tipo_hash, string chave = "");

#endif