/**
* @file	    rastreio.h
* @brief	Declaração do prototipo das funções que realizam o rastreio do diretorio/arquivo
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef RASTREIO_H
#define RASTREIO_H

#include "dados.h"

#include <vector>
using std::vector;

#include <list>
using std::list;

#include <string>
using std::string;

/**
* @brief    Função que verifica se um arquivo foi adicionado ou removido da estrutura
* @param    arquivos Lista dos arquivos para verificação
* @param    dado Arquivo para verificação
* @param    dir Diretorio do arquivo em que a busca está sendo realizada
* @param    count Contador que auxilia em qual subpasta do diretório a busca está sendo realizada
* @return   Se o arquivo foi adicionado ou removido, dependendo da chamada realizada
*/
bool verifica_adicao_remocao_aux (list<Dados> arquivos, Dados dado, vector<string> dir, int count = 0);

/**
* @brief    Função que verifica se um arquivo foi alterado na estrutura
* @param    arquivos Lista dos arquivos para verificação
* @param    dado Arquivo para verificação
* @param    dir Diretorio do arquivo em que a busca está sendo realizada
* @param    count Contador que auxilia em qual subpasta do diretório a busca está sendo realizada
* @return   Se o arquivo foi alterado ou não
*/
bool verifica_alteracao_aux (list<Dados> arquivos, Dados dado, vector<string> dir, int count = 0);

/**
* @brief    Função que verifica as adiçoes e alterações de arquivos dos arquivos antigos para os atuais
* @param    antigos Arquivos da base de dados
* @param    atuais Arquivos atuais
* @param    saida_log Saida do log de rastreio
* @param    dir Diretorio atual da verificação
*/
void verifica_adicao_alteracao (list<Dados> antigos, list<Dados> atuais, string saida_log, vector<string> dir = vector<string> ());

/**
* @brief    Função que verifica as remoções de arquivos dos arquivos antigos para os atuais
* @param    antigos Arquivos da base de dados
* @param    atuais Arquivos atuais
* @param    saida_log Saida do log de rastreio
* @param    dir Diretorio atual da verificação
*/
void verifica_remocao (list<Dados> antigos, list<Dados> atuais, string saida_log, vector<string> dir = vector<string> ());

/**
* @brief    Realiza o rastreio dos arquivos, verificando se houve
            adição, remoção e alteração de arquivos
* @param    arquivos_antigos Arquivos da base de dados
* @param    arquivos_atual Arquivos atuais
* @param    saida_log Saida do log de rastreio
*/
void rastreio(list<Dados> arquivos_antigos, list<Dados> arquivos_atual, string saida_log);

#endif