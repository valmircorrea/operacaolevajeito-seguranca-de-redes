/**
* @file	    dados.h
* @brief	Declaração da estrutura que armazena os dados dos arquivos/diretórios
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef DADOS_H
#define DADOS_H

#include <string>
using std::string;

#include <list>
using std::list;

/**
* Estrutura que armazena os dados dos arquivos/diretórios
*/
typedef struct Dados {
    bool isFile;            /**< Se é arquivo regular ou não */  
    list <Dados> lista;     /**< Se for uma pasta, a lista com seu conteúdo */  
    string nome;            /**< Nome da pasta/arquivo */  
    string hash;            /**< Hash do arquivo */  
} Dados;

#endif