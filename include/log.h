/**
* @file	    log.h
* @brief	Declaração dos protótipos das funções que mostram/gravam os logs do rastreio
* @author   Pedro Emerick (p.emerick@live.com)
* @author   Valmir Correa (valmircsjr@gmail.com)
* @date     09/2018
*/

#ifndef LOG_H
#define LOG_H

#include <string>
using std:: string;

/**
* @brief    Função que grava a mensagem cifrada em um arquivo
* @param    mensagem Mensagem cifrada a ser gravada no arquivo
* @param    nome_arq Nome do arquivo a ser gravado
*/
void gravar_msg (string mensagem, string nome_arq);

/**
* @brief    Imprimi/grava a mensagem de log
* @param    saida Saida para o log, se for "default", sairá na saída padrão do programa
* @param    msg Mensagem de log
*/
void log (string saida, string msg);

#endif