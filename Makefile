LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++
CPPLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR) -I$(INC_DIR)/xml -I$(INC_DIR)/hash

RM = rm -rf
RM_TUDO = rm -fr

PROG = guarda

.PHONY: all clean debug doc doxygen gnuplot init valgrind

all: init $(PROG)

debug: CFLAGS += -g -O0
debug: $(PROG)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

################## prog ###################
$(PROG): $(OBJ_DIR)/main.o $(OBJ_DIR)/hl_md5.o $(OBJ_DIR)/hl_md5wrapper.o $(OBJ_DIR)/log.o $(OBJ_DIR)/tinystr.o $(OBJ_DIR)/tinyxml.o $(OBJ_DIR)/tinyxmlerror.o $(OBJ_DIR)/tinyxmlparser.o $(OBJ_DIR)/manip_arq.o $(OBJ_DIR)/rastreio.o $(OBJ_DIR)/manip_xml.o $(OBJ_DIR)/hmac.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/config.h $(INC_DIR)/manip_arq.h $(INC_DIR)/manip_xml.h $(INC_DIR)/rastreio.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/hl_md5.o: $(SRC_DIR)/hash/hl_md5.cpp $(INC_DIR)/hash/hl_md5.h $(INC_DIR)/hash/hl_types.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/hl_md5wrapper.o: $(SRC_DIR)/hash/hl_md5wrapper.cpp $(INC_DIR)/hash/hl_md5wrapper.h $(INC_DIR)/hash/hl_hashwrapper.h $(INC_DIR)/hash/hl_md5.h $(INC_DIR)/hash/hl_exception.h $(INC_DIR)/hash/hl_types.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/log.o: $(SRC_DIR)/log.cpp $(INC_DIR)/log.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/tinystr.o: $(SRC_DIR)/xml/tinystr.cpp $(INC_DIR)/xml/tinystr.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/tinyxml.o: $(SRC_DIR)/xml/tinyxml.cpp $(INC_DIR)/xml/tinyxml.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/tinyxmlerror.o: $(SRC_DIR)/xml/tinyxmlerror.cpp $(INC_DIR)/xml/tinyxml.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/tinyxmlparser.o: $(SRC_DIR)/xml/tinyxmlparser.cpp $(INC_DIR)/xml/tinyxml.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/manip_arq.o: $(SRC_DIR)/manip_arq.cpp $(INC_DIR)/manip_arq.h $(INC_DIR)/dados.h $(INC_DIR)/hash/hl_md5wrapper.h $(INC_DIR)/hash/hl_hashwrapper.h $(INC_DIR)/hash/hl_md5.h $(INC_DIR)/hash/hl_exception.h $(INC_DIR)/hash/hl_types.h $(INC_DIR)/hmac.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/rastreio.o: $(SRC_DIR)/rastreio.cpp $(INC_DIR)/rastreio.h $(INC_DIR)/dados.h $(INC_DIR)/log.h 
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/manip_xml.o: $(SRC_DIR)/manip_xml.cpp $(INC_DIR)/manip_xml.h $(INC_DIR)/dados.h $(INC_DIR)/xml/tinystr.h $(INC_DIR)/xml/tinyxml.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/hmac.o: $(SRC_DIR)/hmac.cpp $(INC_DIR)/hmac.h $(INC_DIR)/hash/hl_md5wrapper.h $(INC_DIR)/hash/hl_hashwrapper.h $(INC_DIR)/hash/hl_md5.h $(INC_DIR)/hash/hl_exception.h $(INC_DIR)/hash/hl_types.h
	$(CC) -c $(CPPLAGS) -o $@ $<

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*